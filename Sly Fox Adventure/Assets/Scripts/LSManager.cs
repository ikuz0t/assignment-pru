﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LSManager : MonoBehaviour
{
	public LSPlayer thePlayer;

	public AudioSource audioSource;

	private MapPoint[] allPoints;

	// Start is called before the first frame update
	void Start()
	{
		allPoints = FindObjectsOfType<MapPoint>();

		if (PlayerPrefs.HasKey("CurrentLevel"))
		{
			foreach (MapPoint point in allPoints)
			{
				if (point.levelToLoad == PlayerPrefs.GetString("CurrentLevel"))
				{
					thePlayer.transform.position = point.transform.position;
					thePlayer.currentPoint = point;
				}
			}
		}

        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
            audioSource.Play();
        }
        else
        {
            audioSource.Stop();
        }
    }



	public void LoadLevel()
	{
		StartCoroutine(LoadLevelCo());
	}

	public IEnumerator LoadLevelCo()
	{
        string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
        bool isSoundOn = bool.Parse(soundStatus);

        if (isSoundOn)
        {
            AudioManager.instance.PlaySFX(4);
        }

        LSUIController.instance.FadeToBlack();

		yield return new WaitForSeconds((1f / LSUIController.instance.fadeSpeed) + .25f);

		SceneManager.LoadScene(thePlayer.currentPoint.levelToLoad);
	}
}
