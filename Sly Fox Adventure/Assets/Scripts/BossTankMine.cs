﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTankMine : MonoBehaviour
{
    public GameObject explosion;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            Destroy(gameObject);

            Instantiate(explosion, transform.position, transform.rotation);

            PlayerHealthController.instance.DealDamage();

            string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
            bool isSoundOn = bool.Parse(soundStatus);

            if (isSoundOn)
            {
                AudioManager.instance.PlaySFX(3);
            }
        }
    }

    public void Explode()
    {
        Destroy(gameObject);

        string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
        bool isSoundOn = bool.Parse(soundStatus);

        if (isSoundOn)
        {
            AudioManager.instance.PlaySFX(3);
        }

        Instantiate(explosion, transform.position, transform.rotation);
    }
}
